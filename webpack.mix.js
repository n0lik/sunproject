const { mix, config } = require('laravel-mix');



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .js('resources/assets/js/app.js', 'public/js')
   // .less('resources/assets/less/main.less', 'public/css/main.css');
   .less('resources/assets/less/main.less', 'assets/main.css')
   .styles([
       'resources/assets/css/lib/bootstrap.css',
       'public/assets/main.css'
   ], 'public/css/app.css')
    .sourceMaps();
